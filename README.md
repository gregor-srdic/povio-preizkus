# Povio preizkus
Crypto-currency exchange rates display web page.

## Running the project

### `npm install`

Installs the required packages defined in the package.json file.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
The app is ready to be deployed!

## Internationalization
App supports display in english (default) and slovenian languages. To view in slovenian, add `lng=sl` query parameter to url.

## Resources
### CoinMarketCap currency logo
```https://files.coinmarketcap.com/static/img/coins/32x32/${currencyId}.png```      

## Credits
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) and following [Modern React with Redux](https://www.udemy.com/react-redux/learn/v4/overview) course.       
Responsive styling thanks to [Bootstrap](https://getbootstrap.com/).
import axios from 'axios';

export const CURRENCIES_RETURNED = 'currencies_returned';
export const REQUEST_CURRENCIES = 'request_currencies';

export const SET_CURRENCY_DETAILS = 'set_currency_details';
export const CURRENCY_DETAILS_UPDATE_STARTED = 'currency_details_update_started';
export const CURRENCY_DETAILS_UPDATED = 'currency_details_updated';

export const UPDATE_BASE_CURRENCY = 'update_base_currency';

export const ROOT_URL = 'https://api.coinmarketcap.com/v1';

function fetchCurrenciesList(baseCurrency) {
    return axios.get(`${ROOT_URL}/ticker/?${baseCurrency.name === 'USD' ? '' : `convert=${baseCurrency.name}&`}start=0&limit=100`);
}

function fetchSingleCurrencyData(currency, baseCurrency) {
    return axios.get(`${ROOT_URL}/ticker/${currency}/${baseCurrency.name === 'USD' ? '' : `?convert=${baseCurrency.name}`}`);
}

export function fetchCurrenciesData(baseCurrency) {
    return function (dispatch) {
        dispatch({ type: REQUEST_CURRENCIES });
        fetchCurrenciesList(baseCurrency)
            .then(result => {
                dispatch({
                    type: CURRENCIES_RETURNED,
                    payload: result
                });
            })
            .catch(err => {
                dispatch({
                    type: CURRENCIES_RETURNED,
                    payload: null
                });
            });
    };
}

export function updateBaseCurrency(baseCurrency, singleCurrencyId) {
    return function (dispatch) {
        dispatch({
            type: UPDATE_BASE_CURRENCY,
            payload: baseCurrency
        });
        // if page is displaying currency details, update single currency otherwise the whole list
        if (singleCurrencyId !== null) {
            return dispatch(fetchCurrencyDetails(singleCurrencyId, baseCurrency));
        } else {
            return dispatch(fetchCurrenciesData(baseCurrency));
        }
    };
}

export function setCurrencyDetails(currencyDetails, baseCurrency) {
    return function (dispatch) {
        dispatch({
            type: SET_CURRENCY_DETAILS,
            payload: currencyDetails === null ? null : {
                data: currencyDetails,
                baseCurrency
            }
        });
    };
}

export function fetchCurrencyDetails(currencyId, baseCurrency) {
    return function (dispatch) {
        dispatch({
            type: CURRENCY_DETAILS_UPDATE_STARTED,
            payload: {
                currencyId,
                baseCurrency
            }
        });
        fetchSingleCurrencyData(currencyId, baseCurrency)
            .then(result => {
                dispatch({
                    type: CURRENCY_DETAILS_UPDATED,
                    payload: {
                        currencyId,
                        result,
                        baseCurrency
                    }
                });
            })
            .catch(err => {
                dispatch({
                    type: CURRENCY_DETAILS_UPDATED,
                    payload: {
                        currencyId,
                        result: null,
                        baseCurrency
                    }
                });
            });
    };
}
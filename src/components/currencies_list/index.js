import React, { Component } from 'react';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { fetchCurrenciesData, setCurrencyDetails } from '../../actions';
import ChangePercentDisplay from '../utils/change_percent_display';
import CurrencyPrice from '../utils/currency_price_display';
import history from '../../history'
import './style.css';

class CurrenciesList extends Component {

    componentDidMount() {
        // get the fresh currencies list data from api
        this.props.fetchCurrenciesData(this.props.baseCurrency);
    }

    componentWillReceiveProps(newProps) {
        const loadingStatus = !newProps.currencies ? 0 : newProps.currencies.status;
        if (!this.state) {
            this.setState({
                loadingStatus,
                baseCurrency: newProps.baseCurrency,
                currencies: newProps.currencies.data
            });
        } else {
            this.setState({
                loadingStatus,
                baseCurrency: loadingStatus === 0 ? this.state.baseCurrency : newProps.baseCurrency,
                currencies: loadingStatus === 0 ? this.state.currencies : newProps.currencies.data
            });
        }
    }


    goToCurrencyDetails(currency) {
        // use the already available data (will be refreshed when the details page opens)
        this.props.setCurrencyDetails(currency, this.props.baseCurrency);
        history.push({
            pathname: `/currency/${currency.id}`,
            search: this.props.location.search
        });
    }

    renderCurrenciesList() {
        if (!this.state || (this.state.loadingStatus === 0 && !this.state.currencies)) {
            return (
                <tr>
                    <td>{this.props.t('loading')}</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            )
        }
        if (this.props.currencies.status === -1) {
            return (
                <tr>
                    <td>{this.props.t('loadingFailed')}</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            )
        }
        return this.renderCurrenciesListItems();
    }
    renderCurrenciesListItems() {
        return this.state.currencies.map((c, i) => {
            return (
                <tr key={c.id} onClick={() => this.goToCurrencyDetails(c)}>
                    <td >{c.rank}</td>
                    <td className="currencyName">
                        <div className={`currency-icon s-s-${c.id}`}></div>
                        <span>{c.name} ({c.symbol})</span>
                    </td>
                    <td className="text-right">
                        <CurrencyPrice baseCurrency={this.state.baseCurrency} currency={c}></CurrencyPrice><br />
                        <ChangePercentDisplay value={c.percent_change_24h}></ChangePercentDisplay>
                    </td>
                </tr>
            )
        });
    }

    render() {
        return (
            <div>
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">{this.props.t('listView.currency')}</th>
                            <th scope="col" className="text-right">{this.props.t('listView.price')}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderCurrenciesList()}
                    </tbody>
                </table>
                <div className="footer">
                    {this.props.locale === 'en' ? <a href="./?lng=sl">Slovenska verzija</a> : <a href="./?lng=en">English version</a>}
                </div>
            </div>
        );
    }

}

function mapStateToProps({ currencies, baseCurrency, locale }) {
    return { currencies, baseCurrency, locale };
}

export default translate()(connect(mapStateToProps, { fetchCurrenciesData, setCurrencyDetails })(CurrenciesList));

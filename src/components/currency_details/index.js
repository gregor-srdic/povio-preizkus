import React, { Component } from 'react';
import './style.css';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { fetchCurrencyDetails } from '../../actions';
import ChangePercentDisplay from '../utils/change_percent_display';
import CurrencyPrice from '../utils/currency_price_display';
import NumberDisplay from '../utils/number_display';
import * as Spinner from 'react-spinkit';

class CurrencyDetails extends Component {

    currencyId = this.props.match.params.id;

    componentWillReceiveProps(newProps) {
        const loadingStatus = !newProps.selectedCurrency ? 0 : newProps.selectedCurrency.status;
        // component should only refresh view once data has been retrieved or failed
        if (!this.state) {
            this.setState({
                loadingStatus,
                baseCurrency: newProps.selectedCurrency ? newProps.selectedCurrency.baseCurrency : newProps.baseCurrency,
                selectedCurrency: newProps.selectedCurrency
            })
        } else {
            this.setState({
                loadingStatus,
                baseCurrency: loadingStatus === 0 ? this.state.baseCurrency : newProps.selectedCurrency.baseCurrency,
                selectedCurrency: loadingStatus === 0 ? this.state.selectedCurrency : newProps.selectedCurrency
            })
        }
    }

    componentDidMount() {
        this.props.fetchCurrencyDetails(this.currencyId, this.props.baseCurrency);
    }

    render() {
        if (!this.state || (this.state.selectedCurrency.status === 0 && !this.state.selectedCurrency.data)) {
            return (
                <div>{this.props.t('detailsView.loading')}</div>
            )
        }
        if (this.state.selectedCurrency.status === -1) {
            return (
                <div>{this.props.t('detailsView.loadingFailed')}</div>
            )
        }
        const selectedCurrency = this.state.selectedCurrency.data,
            baseCurrency = this.state.selectedCurrency.baseCurrency;
        return (
            <div className="currency-details-component">
                <div className="page-header">
                    <div className="icon-wrapper">
                        <img alt={selectedCurrency.name} src={`https://files.coinmarketcap.com/static/img/coins/32x32/${selectedCurrency.id}.png`} />
                        {(this.state.loadingStatus === 0) ? <Spinner name="cube-grid" /> : ''}
                    </div>
                    <span>{selectedCurrency.name}</span>
                    <span>({selectedCurrency.symbol})</span>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <th>{this.props.t('detailsView.currentPrice')}</th>
                            <td><CurrencyPrice baseCurrency={baseCurrency} currency={selectedCurrency}></CurrencyPrice></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.btcPrice')}</th>
                            <td>{selectedCurrency.price_btc}</td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.hourlyChange')}</th>
                            <td><ChangePercentDisplay value={selectedCurrency.percent_change_1h}></ChangePercentDisplay></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.dailyChange')}</th>
                            <td><ChangePercentDisplay value={selectedCurrency.percent_change_24h}></ChangePercentDisplay></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.weeklyChange')}</th>
                            <td><ChangePercentDisplay value={selectedCurrency.percent_change_7d}></ChangePercentDisplay></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.marketCap')}</th>
                            <td><CurrencyPrice prefix="market_cap_" baseCurrency={baseCurrency} currency={selectedCurrency}></CurrencyPrice></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.volume24h')}</th>
                            <td><CurrencyPrice prefix="24h_volume_" baseCurrency={baseCurrency} currency={selectedCurrency}></CurrencyPrice></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.availableSupply')}</th>
                            <td><NumberDisplay value={selectedCurrency.available_supply}></NumberDisplay></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.totalSupply')}</th>
                            <td><NumberDisplay value={selectedCurrency.total_supply}></NumberDisplay></td>
                        </tr>
                        <tr>
                            <th>{this.props.t('detailsView.maxSupply')}</th>
                            <td><NumberDisplay value={selectedCurrency.max_supply}></NumberDisplay></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

function mapStateToProps({ selectedCurrency, baseCurrency, locale }, props) {
    const currencyId = props.match.params.id;
    return {
        baseCurrency,
        selectedCurrency: selectedCurrency && selectedCurrency.currencyId === currencyId ? selectedCurrency : null
    };
}

export default translate()(connect(mapStateToProps, { fetchCurrencyDetails })(CurrencyDetails));

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { translate } from 'react-i18next';
import { AVAILABLE_CURRENCIES } from '../../constants';
import { updateBaseCurrency } from '../../actions';
import './style.css';

class Header extends Component {

    selectBaseCurrency(baseCurrency) {
        // pass the selected currency id parameter, if page is currently displaying single currency details
        this.props.updateBaseCurrency(baseCurrency, this.props.selectedCurrency ? this.props.selectedCurrency.currencyId : null);
    }

    renderBaseCurrencyOptions() {
        return AVAILABLE_CURRENCIES.filter(el => el.name !== this.props.baseCurrency.name).map((el) => {
            return (
                <button className="dropdown-item" type="button" key={el.name} onClick={() => this.selectBaseCurrency(el)}>{el.name} {el.symbol}</button>
            )
        });
    }

    // renderSingleCurrencyTitle() {
    //     if (this.props.selectedCurrency && this.props.selectedCurrency.data) {
    //         return (
    //             <div className="navbar-brand">
    //                 &bull;&nbsp;&nbsp;<span>{this.props.selectedCurrency.data.name}</span>
    //             </div>
    //         );
    //     }
    //     return '';
    // }

    render() {
        return (
            <header>
                <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                    <Link className="navbar-brand" to={{ pathname: '/', search: this.props.location.search }}>{this.props.t('title')}</Link>
                    <div className="nav-item ml-auto dropdown dropdown-right">
                        <button type="button" className="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {this.props.baseCurrency.name} {this.props.baseCurrency.symbol}
                        </button>
                        <div className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            {this.renderBaseCurrencyOptions()}
                        </div>
                    </div>
                </nav >
            </header >
        );
    }
}

function mapStateToProps({ selectedCurrency, baseCurrency }) {
    return { selectedCurrency, baseCurrency };
}

export default translate()(connect(mapStateToProps, { updateBaseCurrency })(Header));

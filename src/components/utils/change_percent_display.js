import React, { Component } from 'react';
import { connect } from 'react-redux';

class ChangePercentDisplay extends Component {

    render() {
        const value = parseFloat(this.props.value / 100);
        const percentage = new Intl.NumberFormat(this.props.locale, {
            style: 'percent',
            minimumFractionDigits: 2,
            maximumFractionDigits: 2
        })
            .format(value);
        return (<span className={(value > 0 ? 'positive' : 'negative')}>{percentage}</span>);
    }

}


function mapStateToProps({ locale }, props) {
    return { locale };
}

export default connect(mapStateToProps, null)(ChangePercentDisplay);

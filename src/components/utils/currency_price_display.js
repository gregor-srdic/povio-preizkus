import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getCurrencyPriceInBaseCurrency } from '../../utils';

class CurrencyPrice extends Component {

    render() {
        const price = new Intl.NumberFormat(this.props.locale, {
            style: 'currency',
            currency: this.props.baseCurrency.name
        })
            .format(getCurrencyPriceInBaseCurrency(this.props.currency, this.props.baseCurrency, this.props.prefix));
        return (<span>{price}</span>);
    }

}


function mapStateToProps({ locale }, props) {
    return { locale };
}

export default connect(mapStateToProps, null)(CurrencyPrice);

import React, { Component } from 'react';
import { connect } from 'react-redux';

class NumberDisplay extends Component {

    render() {
        const value = !this.props.value ? 'N/A' : new Intl.NumberFormat(
            this.props.locale,
            {}
        )
            .format(this.props.value);
        return (<span>{value}</span>);
    }

}


function mapStateToProps({ locale }, props) {
    return { locale };
}

export default connect(mapStateToProps, null)(NumberDisplay);

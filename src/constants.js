export const LOCAL_STORAGE_KEYS = {
    baseCurrency: 'base_currency'
};

export const AVAILABLE_CURRENCIES = [
    {
        name: "USD",
        symbol: "$"
    },
    {
        name: "EUR",
        symbol: "€"
    },
    {
        name: "CNY",
        symbol: "¥"
    },
    {
        name: "JPY",
        symbol: "¥"
    },
    {
        name: "RUB",
        symbol: "₽"
    }

    // "AUD",
    // "BRL",
    // "CAD",
    // "CHF",
    // "CLP",
    // "CZK",
    // "DKK",
    // "GBP",
    // "HKD",
    // "HUF",
    // "IDR",
    // "ILS",
    // "INR",
    // "KRW",
    // "MXN",
    // "MYR",
    // "NOK",
    // "NZD",
    // "PHP",
    // "PKR",
    // "PLN",
    // "SEK",
    // "SGD",
    // "THB",
    // "TRY",
    // "TWD",
    // "ZAR"
];
import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { reactI18nextModule } from 'react-i18next';

export default function configureI18n(lng) {

    return i18n
        .use(XHR)
        .use(LanguageDetector)
        .use(reactI18nextModule)
        .init({
            fallbackLng: 'en',
            lng: lng ? lng : 'en',
            debug: false,
            load: 'languageOnly',
            interpolation: {
                escapeValue: false,
            },
            react: {
                wait: true,
                bindI18n: 'languageChanged loaded',
                bindStore: 'added removed',
                nsMode: 'default'
            }
        });

}
import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Switch } from 'react-router-dom';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import { ConnectedRouter, routerMiddleware, routerReducer } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk';
import { I18n } from 'react-i18next';

import registerServiceWorker from './registerServiceWorker';

import { getUrlQueryParam } from './utils';
import rootReducer from './reducers';
import createLocaleReducer from './reducers/reducer_locale';
import history from './history';
import i18n from './i18n';

import Header from './components/header';
import CurrenciesList from './components/currencies_list';
import CurrencyDetails from './components/currency_details';
import { LOCAL_STORAGE_KEYS } from './constants';

import './icons.css';
import './index.css';

const lng = getUrlQueryParam('lng') || 'en';
const reduxRouterMiddleware = routerMiddleware(history);
const store = createStore(
    combineReducers({
        ...rootReducer,
        router: routerReducer,
        locale: createLocaleReducer(lng)
    }),
    applyMiddleware(reduxRouterMiddleware, thunkMiddleware)
);

i18n(lng);

// persist selected base currency to local storage 
store.subscribe(() => {
    const baseCurrency = store.getState().baseCurrency;
    if (baseCurrency) {
        window.localStorage.setItem(LOCAL_STORAGE_KEYS.baseCurrency, JSON.stringify(baseCurrency));
    }
});

ReactDOM.render(
    <I18n>
        {
            (t, { i18n }) => (
                <Provider store={store}>
                    <ConnectedRouter history={history}>
                        <div>
                            <Route path="/" component={Header} />
                            <main className="container">
                                <Switch>
                                    <Route path="/currency/:id" component={CurrencyDetails} />
                                    <Route path="/" component={CurrenciesList} />
                                </Switch>
                            </main>
                        </div>
                    </ConnectedRouter>
                </Provider>
            )
        }
    </I18n>,
    document.getElementById('root')
);

registerServiceWorker();
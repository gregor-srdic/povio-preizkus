import CurrenciesReducer from "./reducer_currencies";
import BaseCurrencyReducer from "./reducer_base_currency";
import SelectedCurrencyReducer from "./reducer_selected_currency";

const rootReducer = {
    currencies: CurrenciesReducer,
    baseCurrency: BaseCurrencyReducer,
    selectedCurrency: SelectedCurrencyReducer
};

export default rootReducer;

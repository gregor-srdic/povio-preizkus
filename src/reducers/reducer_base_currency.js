import { UPDATE_BASE_CURRENCY } from '../actions';
import { AVAILABLE_CURRENCIES, LOCAL_STORAGE_KEYS } from '../constants';

// get initial base currency from local storage if available, otherwise use first from available currencies list
const initialBaseCurrency = window.localStorage.getItem(LOCAL_STORAGE_KEYS.baseCurrency) ? JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_KEYS.baseCurrency)) : AVAILABLE_CURRENCIES[0];

export default function (state = initialBaseCurrency, action) {
    switch (action.type) {
        case UPDATE_BASE_CURRENCY:
            return action.payload;
        default:
            return state;
    }
}
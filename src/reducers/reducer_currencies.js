import { REQUEST_CURRENCIES, CURRENCIES_RETURNED } from '../actions';

export default function (state = null, action) {
    switch (action.type) {
        case CURRENCIES_RETURNED:
            if (action.payload) {
                return {
                    status: action.payload.status === 200 ? 1 : -1,
                    data: action.payload.data
                };
            }
            return {
                status: -1,
                data: null
            };
        case REQUEST_CURRENCIES:
            return {
                status: 0,
                data: null
            };
        default:
            return state;
    }
}
import { SET_CURRENCY_DETAILS, CURRENCY_DETAILS_UPDATE_STARTED, CURRENCY_DETAILS_UPDATED } from '../actions';
import { LOCATION_CHANGE } from 'react-router-redux';

export default function (state = null, action) {
    switch (action.type) {
        case LOCATION_CHANGE:
            if (!/^\/currency\//.test(action.payload.pathname)) {
                // if we are not on currency details page, clear the selected currency state
                return null;
            }
            return state;
        case SET_CURRENCY_DETAILS:
            if (action.payload === null) {
                return null;
            }
            return {
                status: 0,
                currencyId: action.payload ? action.payload.data.id : null,
                data: action.payload.data,
                baseCurrency: action.payload.baseCurrency
            };
        case CURRENCY_DETAILS_UPDATE_STARTED:
            if (state && state.currencyId === action.payload.currencyId) {
                const newState = { ...state };
                newState.status = 0;
                newState.baseCurrency = action.payload.baseCurrency;
                return newState;
            }
            return {
                status: 0,
                currencyId: action.payload.currencyId,
                baseCurrency: action.payload.baseCurrency,
                data: null
            };
        case CURRENCY_DETAILS_UPDATED:
            const status = action.payload.result && action.payload.result.status === 200 ? 1 : -1;
            const newState = (state && state.currencyId === action.payload.currencyId) ? state : {};
            newState.status = status;
            newState.baseCurrency = action.payload.baseCurrency;
            if (status === 1) {
                newState.data = action.payload.result.data[0];
            }
            return { ...newState };
        default:
            return state;
    }
}
// returns value in base currency based on exchange rate, calculated from usd price and base currency price
export function getValueInBaseCurrency(usdPrice, baseCurrencyPrice, usdValue) {
    if (usdPrice && baseCurrencyPrice && usdValue) {
        return (Math.round((parseFloat(baseCurrencyPrice) * parseFloat(usdValue) * 100) / parseFloat(usdPrice)) / 100).toFixed(2);
    }
    return null;
}
// returns currency price in selected base currency
export function getCurrencyPriceInBaseCurrency(currency, baseCurrency, prefix) {
    if (!prefix) {
        prefix = 'price_';
    }
    return currency[`${prefix}${baseCurrency.name.toLowerCase()}`];
}
export function getUrlQueryParam(name) {
    var results = new RegExp('[?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return decodeURI(results[1]) || 0;
    }
}